# Translation of powerdevil.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2009, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-24 00:40+0000\n"
"PO-Revision-Date: 2017-03-19 22:40+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Associated-UI-Catalogs: powerdevilprofilesconfig kdelibs4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Часлав Илић"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "caslav.ilic@gmx.net"

#: actions/bundled/brightnesscontrol.cpp:39 actions/bundled/dpms.cpp:72
#: actions/bundled/handlebuttonevents.cpp:42
#: actions/bundled/keyboardbrightnesscontrol.cpp:45
#, kde-format
msgctxt "Name for powerdevil shortcuts category"
msgid "Power Management"
msgstr "Управљање напајањем"

#: actions/bundled/brightnesscontrol.cpp:42
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness"
msgstr "Повећај осветљај екрана"

#: actions/bundled/brightnesscontrol.cpp:47
#, fuzzy, kde-format
#| msgctxt "@action:inmenu Global shortcut"
#| msgid "Increase Screen Brightness"
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness by 1%"
msgstr "Повећај осветљај екрана"

#: actions/bundled/brightnesscontrol.cpp:52
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness"
msgstr "Смањи осветљај екрана"

#: actions/bundled/brightnesscontrol.cpp:57
#, fuzzy, kde-format
#| msgctxt "@action:inmenu Global shortcut"
#| msgid "Decrease Screen Brightness"
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness by 1%"
msgstr "Смањи осветљај екрана"

# >> @item:inlistbox
#: actions/bundled/dpms.cpp:75
#, fuzzy, kde-format
#| msgid "Turn off screen"
msgctxt "@action:inmenu Global shortcut"
msgid "Turn Off Screen"
msgstr "угаси екран"

#: actions/bundled/handlebuttonevents.cpp:47
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Suspend"
msgstr "Суспендуј"

#: actions/bundled/handlebuttonevents.cpp:52
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Hibernate"
msgstr "У хибернацију"

#: actions/bundled/handlebuttonevents.cpp:57
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Power Off"
msgstr "Угаси"

#: actions/bundled/handlebuttonevents.cpp:71
#, fuzzy, kde-format
#| msgctxt "@action:inmenu Global shortcut"
#| msgid "Power Off"
msgctxt ""
"@action:inmenu Global shortcut, used for long presses of the power button"
msgid "Power Down"
msgstr "Угаси"

#: actions/bundled/keyboardbrightnesscontrol.cpp:49
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Keyboard Brightness"
msgstr "Повећај осветљај тастатуре"

#: actions/bundled/keyboardbrightnesscontrol.cpp:54
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Keyboard Brightness"
msgstr "Смањи осветљај тастатуре"

#: actions/bundled/keyboardbrightnesscontrol.cpp:59
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Toggle Keyboard Backlight"
msgstr "Обрни осветљење тастатуре"

#: powerdevilapp.cpp:53
#, fuzzy, kde-format
#| msgctxt "Name for powerdevil shortcuts category"
#| msgid "Power Management"
msgid "KDE Power Management System"
msgstr "Управљање напајањем"

#: powerdevilapp.cpp:55
#, kde-format
msgctxt "@title"
msgid ""
"PowerDevil, an advanced, modular and lightweight power management daemon"
msgstr ""

#: powerdevilapp.cpp:57
#, kde-format
msgctxt "@info:credit"
msgid "(c) 2015-2019 Kai Uwe Broulik"
msgstr ""

#: powerdevilapp.cpp:58
#, kde-format
msgctxt "@info:credit"
msgid "Kai Uwe Broulik"
msgstr ""

#: powerdevilapp.cpp:58
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr ""

#: powerdevilapp.cpp:59
#, kde-format
msgctxt "@info:credit"
msgid "Dario Freddi"
msgstr ""

#: powerdevilapp.cpp:59
#, kde-format
msgctxt "@info:credit"
msgid "Previous maintainer"
msgstr ""

#: powerdevilapp.cpp:101
#, fuzzy, kde-format
#| msgid "Switch off after"
msgid "Switch Power Profile"
msgstr "Угаси после"

#: powerdevilapp.cpp:116
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: powerdevilcore.cpp:380 powerdevilcore.cpp:391
#, kde-format
msgid "Activity Manager"
msgstr "Менаџер активности"

#: powerdevilcore.cpp:381
#, fuzzy, kde-format
#| msgid "This activity's policies prevent the system from suspending"
msgid "This activity's policies prevent the system from going to sleep"
msgstr "Смернице ове активности не допуштају суспендовање система"

#: powerdevilcore.cpp:392
#, kde-format
msgid "This activity's policies prevent screen power management"
msgstr "Смернице ове активности не допуштају управљање напајањем екрана"

#: powerdevilcore.cpp:454
#, kde-format
msgid "Extra Battery Added"
msgstr "Додата допунска батерија"

#: powerdevilcore.cpp:455 powerdevilcore.cpp:676
#, kde-format
msgid "The computer will no longer go to sleep."
msgstr ""

#: powerdevilcore.cpp:520
#, kde-format
msgctxt "%1 is vendor name, %2 is product name"
msgid "%1 %2"
msgstr "%1 %2"

#: powerdevilcore.cpp:523
#, kde-format
msgctxt "The battery in an external device"
msgid "Device Battery Low (%1% Remaining)"
msgstr "Батерија у уређају при крају (остало %1%)"

#: powerdevilcore.cpp:525
#, fuzzy, kde-format
#| msgctxt "Placeholder is device name"
#| msgid ""
#| "The battery in your mouse (\"%1\") is low, and the device may turn itself "
#| "off at any time. Please replace or charge the battery as soon as possible."
msgctxt "Placeholder is device name"
msgid ""
"The battery in \"%1\" is running low, and the device may turn off at any "
"time. Please recharge or replace the battery."
msgstr ""
"Батерија у мишу („%1“) при крају је, те се уређај сваког часа може сам "
"угасити. Што пре замените или допуните батерију."

#: powerdevilcore.cpp:532
#, kde-format
msgid "Mouse Battery Low (%1% Remaining)"
msgstr "Батерија миша при крају (остало %1%)"

#: powerdevilcore.cpp:536
#, kde-format
msgid "Keyboard Battery Low (%1% Remaining)"
msgstr "Батерија тастатуре при крају (остало %1%)"

#: powerdevilcore.cpp:540
#, fuzzy, kde-format
#| msgctxt "The battery in an external device"
#| msgid "Device Battery Low (%1% Remaining)"
msgid "Bluetooth Device Battery Low (%1% Remaining)"
msgstr "Батерија у уређају при крају (остало %1%)"

#: powerdevilcore.cpp:542
#, fuzzy, kde-format
#| msgctxt "Placeholder is device name"
#| msgid ""
#| "The battery in your mouse (\"%1\") is low, and the device may turn itself "
#| "off at any time. Please replace or charge the battery as soon as possible."
msgctxt "Placeholder is device name"
msgid ""
"The battery in Bluetooth device \"%1\" is running low, and the device may "
"turn off at any time. Please recharge or replace the battery."
msgstr ""
"Батерија у мишу („%1“) при крају је, те се уређај сваког часа може сам "
"угасити. Што пре замените или допуните батерију."

#: powerdevilcore.cpp:584
#, fuzzy, kde-format
#| msgid ""
#| "Your battery is low. If you need to continue using your computer, either "
#| "plug in your computer, or shut it down and then change the battery."
msgid "Ensure that the power adapter is plugged in and provides enough power."
msgstr ""
"Батерија је при крају. Ако желите да наставите са радом, или утакните "
"рачунар у струју или га искључите и промените батерију."

#: powerdevilcore.cpp:586
#, kde-format
msgid "Plug in the computer."
msgstr ""

#: powerdevilcore.cpp:605
#, fuzzy, kde-format
#| msgid ""
#| "Your battery level is critical, the computer will be halted in 60 seconds."
msgid "Battery level critical. Your computer will shut down in 60 seconds."
msgstr "Батерија је на критичном нивоу, рачунар ће бити угашен за 60 секунди."

# >> @item:inlistbox
#: powerdevilcore.cpp:607
#, fuzzy, kde-format
#| msgid "Shut down"
msgctxt ""
"@action:button Shut down without waiting for the battery critical timer"
msgid "Shut Down Now"
msgstr "угаси"

#: powerdevilcore.cpp:613
#, fuzzy, kde-format
#| msgid ""
#| "Your battery level is critical, the computer will be hibernated in 60 "
#| "seconds."
msgid ""
"Battery level critical. Your computer will enter hibernation mode in 60 "
"seconds."
msgstr ""
"Батерија је на критичном нивоу, рачунар ће бити послат у хибернацију за 60 "
"секунди."

# >> @item:inlistbox
#: powerdevilcore.cpp:615
#, fuzzy, kde-format
#| msgid "Hibernate"
msgctxt ""
"@action:button Enter hibernation mode without waiting for the battery "
"critical timer"
msgid "Hibernate Now"
msgstr "у хибернацију"

#: powerdevilcore.cpp:621
#, fuzzy, kde-format
#| msgid ""
#| "Your battery level is critical, the computer will be halted in 60 seconds."
msgid "Battery level critical. Your computer will go to sleep in 60 seconds."
msgstr "Батерија је на критичном нивоу, рачунар ће бити угашен за 60 секунди."

#: powerdevilcore.cpp:623
#, kde-format
msgctxt ""
"@action:button Suspend to ram without waiting for the battery critical timer"
msgid "Sleep Now"
msgstr ""

#: powerdevilcore.cpp:629
#, fuzzy, kde-format
#| msgid "Your battery level is critical, save your work as soon as possible."
msgid "Please save your work."
msgstr "Батерија је на критичном нивоу, сачувајте свој рад што је пре могуће."

#: powerdevilcore.cpp:635
#, fuzzy, kde-format
#| msgctxt ""
#| "Cancel timeout that will automatically suspend system because of low "
#| "battery"
#| msgid "Cancel"
msgctxt ""
"Cancel timeout that will automatically put system to sleep because of low "
"battery"
msgid "Cancel"
msgstr "Одустани"

#: powerdevilcore.cpp:647
#, kde-format
msgid "Battery Low (%1% Remaining)"
msgstr "Батерија при крају (остало %1%)"

#: powerdevilcore.cpp:651
#, kde-format
msgid "Battery Critical (%1% Remaining)"
msgstr "Батерија критична (остало %1%)"

#: powerdevilcore.cpp:675
#, kde-format
msgid "AC Adapter Plugged In"
msgstr "Струјни адаптер утакнут"

#: powerdevilcore.cpp:678
#, kde-format
msgid "Running on AC power"
msgstr "На зидном напајању"

#: powerdevilcore.cpp:678
#, fuzzy, kde-format
#| msgid "The power adaptor has been plugged in."
msgid "The power adapter has been plugged in."
msgstr "Струјни адаптер је утакнут."

#: powerdevilcore.cpp:681
#, kde-format
msgid "Running on Battery Power"
msgstr "На батеријском напајању"

#: powerdevilcore.cpp:681
#, fuzzy, kde-format
#| msgid "The power adaptor has been unplugged."
msgid "The power adapter has been unplugged."
msgstr "Струјни адаптер је извучен."

#: powerdevilcore.cpp:742
#, fuzzy, kde-format
#| msgid "Charge Complete"
msgid "Charging Complete"
msgstr "Пуњење довршено"

#: powerdevilcore.cpp:742
#, fuzzy, kde-format
#| msgid "Your battery is now fully charged."
msgid "Battery now fully charged."
msgstr "Батерија је сада потпуно напуњена."

#~ msgctxt "Brightness level, label for the slider"
#~ msgid "Level"
#~ msgstr "Ниво"

#~ msgid " min"
#~ msgstr " мин."

# >> @item:inlistbox
#~ msgid "After"
#~ msgstr "после"

#~ msgid "Switch off after"
#~ msgstr "Угаси после"

#~ msgctxt ""
#~ "Execute action on lid close even when external monitor is connected"
#~ msgid "Even when an external monitor is connected"
#~ msgstr "Чак и кад је прикључен спољашњи монитор"

# >> @item:inlistbox
#~ msgid "Do nothing"
#~ msgstr "не ради ништа"

# >> @item:inlistbox
#~ msgid "Hibernate"
#~ msgstr "у хибернацију"

# >> @item:inlistbox
#~ msgid "Shut down"
#~ msgstr "угаси"

# >> @item:inlistbox
#~ msgid "Lock screen"
#~ msgstr "закључај екран"

# >> @item:inlistbox
#~ msgid "Prompt log out dialog"
#~ msgstr "покажи одјавни дијалог"

# >> @item:inlistbox
#~ msgid "Turn off screen"
#~ msgstr "угаси екран"

#~ msgid "When laptop lid closed"
#~ msgstr "Када се спусти поклопац"

#~ msgid "When power button pressed"
#~ msgstr "Када се притисне дугме напајања"

#~ msgctxt "@label:slider Brightness level"
#~ msgid "Level"
#~ msgstr "Ниво"

# >> @item:inlistbox
#~ msgid "Leave unchanged"
#~ msgstr "остави како је"

#, fuzzy
#~| msgctxt "@action:inmenu Global shortcut"
#~| msgid "Power Off"
#~ msgid "Power Save"
#~ msgstr "Угаси"

#, fuzzy
#~| msgid "Switch off after"
#~ msgctxt "Switch to power management profile"
#~ msgid "Switch to:"
#~ msgstr "Угаси после"

#~ msgid "Script"
#~ msgstr "Скрипта"

#~ msgid "Run script"
#~ msgstr "Изврши скрипту"

# >> @item:inlistbox
#, fuzzy
#~| msgid "After"
#~ msgid "after "
#~ msgstr "после"

# >> @item:inlistbox
#, fuzzy
#~| msgid "Hybrid suspend"
#~ msgid "Hybrid sleep"
#~ msgstr "у хибридну хибернацију"

# >> @item:inlistbox
#~ msgid "On Profile Load"
#~ msgstr "при учитавању профила"

# >> @item:inlistbox
#~ msgid "On Profile Unload"
#~ msgstr "при укидању профила"

#~ msgid "Unsupported suspend method"
#~ msgstr "Неподржан метод суспендовања"

#, fuzzy
#~| msgid ""
#~| "Your battery is low. If you need to continue using your computer, either "
#~| "plug in your computer, or shut it down and then change the battery."
#~ msgid ""
#~ "Battery running low - to continue using your computer, plug it in or shut "
#~ "it down and change the battery."
#~ msgstr ""
#~ "Батерија је при крају. Ако желите да наставите са радом, или утакните "
#~ "рачунар у струју или га искључите и промените батерију."

# >> @item:inlistbox
#~ msgid "Turn off"
#~ msgstr "искључи"

# >> @item:inlistbox
#~ msgid "Turn on"
#~ msgstr "укључи"

# >> @item
#~ msgid "Wi-Fi"
#~ msgstr "бежична"

# >> @item
#~ msgid "Mobile broadband"
#~ msgstr "мобилна широкопојасна"

# >> @item
#~ msgid "Bluetooth"
#~ msgstr "блутут"
