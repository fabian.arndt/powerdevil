# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the powerdevil package.
#
# SPDX-FileCopyrightText: 2024 Kisaragi Hiu <mail@kisaragi-hiu.com>
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-11 00:39+0000\n"
"PO-Revision-Date: 2024-03-20 18:04+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@lists.slat.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 24.04.70\n"

#: osdaction.cpp:23
#, kde-format
msgctxt "power profile"
msgid "Power Save Mode"
msgstr "省電模式"

#: osdaction.cpp:24
#, kde-format
msgctxt "power profile"
msgid "Balanced Performance Mode"
msgstr "平衡效能模式"

#: osdaction.cpp:25
#, kde-format
msgctxt "power profile"
msgid "Maximum Performance Mode"
msgstr "最高效能模式"
