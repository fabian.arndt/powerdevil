# Diego Iastrubni <elcuco@kde.org>, 2009.
# Tahmar1900 <tahmar1900@gmail.com>, 2011.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023, 2024 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-24 00:40+0000\n"
"PO-Revision-Date: 2024-04-08 18:45+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "netanel_h,אלקנה ברדוגו,צוות התרגום של KDE ישראל"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "netanel@gmx.com,ttv200@gmail.com,kde-l10n-he@kde.org"

#: actions/bundled/brightnesscontrol.cpp:39 actions/bundled/dpms.cpp:72
#: actions/bundled/handlebuttonevents.cpp:42
#: actions/bundled/keyboardbrightnesscontrol.cpp:45
#, kde-format
msgctxt "Name for powerdevil shortcuts category"
msgid "Power Management"
msgstr "ניהול צריכת חשמל"

#: actions/bundled/brightnesscontrol.cpp:42
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness"
msgstr "הבהרת המסך"

#: actions/bundled/brightnesscontrol.cpp:47
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Screen Brightness by 1%"
msgstr "הבהרת המסך ב־1%"

#: actions/bundled/brightnesscontrol.cpp:52
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness"
msgstr "החשכת המסך"

#: actions/bundled/brightnesscontrol.cpp:57
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Screen Brightness by 1%"
msgstr "החשכת המסך ב־1%"

#: actions/bundled/dpms.cpp:75
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Turn Off Screen"
msgstr "כיבוי המסך"

#: actions/bundled/handlebuttonevents.cpp:47
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Suspend"
msgstr "השהיה"

#: actions/bundled/handlebuttonevents.cpp:52
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Hibernate"
msgstr "תרדמת"

#: actions/bundled/handlebuttonevents.cpp:57
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Power Off"
msgstr "כיבוי"

#: actions/bundled/handlebuttonevents.cpp:71
#, kde-format
msgctxt ""
"@action:inmenu Global shortcut, used for long presses of the power button"
msgid "Power Down"
msgstr "כיבוי"

#: actions/bundled/keyboardbrightnesscontrol.cpp:49
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Increase Keyboard Brightness"
msgstr "הגברת תאורת מקלדת"

#: actions/bundled/keyboardbrightnesscontrol.cpp:54
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Decrease Keyboard Brightness"
msgstr "החשכת תאורת מקלדת"

#: actions/bundled/keyboardbrightnesscontrol.cpp:59
#, kde-format
msgctxt "@action:inmenu Global shortcut"
msgid "Toggle Keyboard Backlight"
msgstr "הפעלה/כיבוי של תאורת מקלדת"

#: powerdevilapp.cpp:53
#, kde-format
msgid "KDE Power Management System"
msgstr "מערכת ניהול צריכת החשמל של KDE"

#: powerdevilapp.cpp:55
#, kde-format
msgctxt "@title"
msgid ""
"PowerDevil, an advanced, modular and lightweight power management daemon"
msgstr "PowerDevil, סוכן ניהול צריכת חשמל מתקדם, גמיש וקליל"

#: powerdevilapp.cpp:57
#, kde-format
msgctxt "@info:credit"
msgid "(c) 2015-2019 Kai Uwe Broulik"
msgstr "(c) 2015-2019 קאי אווה ברוליק"

#: powerdevilapp.cpp:58
#, kde-format
msgctxt "@info:credit"
msgid "Kai Uwe Broulik"
msgstr "קאי אווה ברוליק"

#: powerdevilapp.cpp:58
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "מתחזק"

#: powerdevilapp.cpp:59
#, kde-format
msgctxt "@info:credit"
msgid "Dario Freddi"
msgstr "דאריו פרדי"

#: powerdevilapp.cpp:59
#, kde-format
msgctxt "@info:credit"
msgid "Previous maintainer"
msgstr "מתחזק קודם"

#: powerdevilapp.cpp:101
#, kde-format
msgid "Switch Power Profile"
msgstr "החלפת פרופיל צריכת חשמל"

#: powerdevilapp.cpp:116
#, kde-format
msgid "Replace an existing instance"
msgstr "החלפת עותק קיים"

#: powerdevilcore.cpp:380 powerdevilcore.cpp:391
#, kde-format
msgid "Activity Manager"
msgstr "מנהל פעילויות"

#: powerdevilcore.cpp:381
#, kde-format
msgid "This activity's policies prevent the system from going to sleep"
msgstr "מדיניויות הפעילות מונעות מהמערכת לישון"

#: powerdevilcore.cpp:392
#, kde-format
msgid "This activity's policies prevent screen power management"
msgstr "מדיניויות הפעילות הזו מונעות את ניהול צריכת החשמל של המסך"

#: powerdevilcore.cpp:454
#, kde-format
msgid "Extra Battery Added"
msgstr "נוספה סוללה נוספת"

#: powerdevilcore.cpp:455 powerdevilcore.cpp:676
#, kde-format
msgid "The computer will no longer go to sleep."
msgstr "המחשב לא יישן עוד."

#: powerdevilcore.cpp:520
#, kde-format
msgctxt "%1 is vendor name, %2 is product name"
msgid "%1 %2"
msgstr "%1 %2"

#: powerdevilcore.cpp:523
#, kde-format
msgctxt "The battery in an external device"
msgid "Device Battery Low (%1% Remaining)"
msgstr "סוללת ההתקן נמוכה (נותרו %1% אחוז)"

#: powerdevilcore.cpp:525
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in \"%1\" is running low, and the device may turn off at any "
"time. Please recharge or replace the battery."
msgstr ""
"עוצמת הסוללה של „%1” נמוכה, וההתקן עשוי לכבות בכל רגע. נא להחליף סוללות או "
"להטעין אותו."

#: powerdevilcore.cpp:532
#, kde-format
msgid "Mouse Battery Low (%1% Remaining)"
msgstr "עוצמת סוללת העכבר נמוכה (נותרו %1% אחוז)"

#: powerdevilcore.cpp:536
#, kde-format
msgid "Keyboard Battery Low (%1% Remaining)"
msgstr "עוצמת סוללת המקלדת נמוכה (נותרו %1% אחוז)"

#: powerdevilcore.cpp:540
#, kde-format
msgid "Bluetooth Device Battery Low (%1% Remaining)"
msgstr "עוצמת סוללת התקן הבלוטות׳ נמוכה (נותרו %1% אחוז)"

#: powerdevilcore.cpp:542
#, kde-format
msgctxt "Placeholder is device name"
msgid ""
"The battery in Bluetooth device \"%1\" is running low, and the device may "
"turn off at any time. Please recharge or replace the battery."
msgstr ""
"עוצמת הסוללה של התקן הבלוטות׳ שלך „%1” נמוכה, וההתקן עשוי לכבות בכל רגע. נא "
"להחליף סוללות או להטעין אותו."

#: powerdevilcore.cpp:584
#, kde-format
msgid "Ensure that the power adapter is plugged in and provides enough power."
msgstr "נא לוודא שהמטען מחובר ומספק מספיק חשמל."

#: powerdevilcore.cpp:586
#, kde-format
msgid "Plug in the computer."
msgstr "לחבר את המחשב."

#: powerdevilcore.cpp:605
#, kde-format
msgid "Battery level critical. Your computer will shut down in 60 seconds."
msgstr "מצב הסוללה קריטי. המחשב ייכבה בעוד 60 שניות."

#: powerdevilcore.cpp:607
#, kde-format
msgctxt ""
"@action:button Shut down without waiting for the battery critical timer"
msgid "Shut Down Now"
msgstr "לכבות כעת"

#: powerdevilcore.cpp:613
#, kde-format
msgid ""
"Battery level critical. Your computer will enter hibernation mode in 60 "
"seconds."
msgstr "מצב הסוללה קריטי. המחשב ייכנס לתרדמת בעוד 60 שניות."

#: powerdevilcore.cpp:615
#, kde-format
msgctxt ""
"@action:button Enter hibernation mode without waiting for the battery "
"critical timer"
msgid "Hibernate Now"
msgstr "להרדים כעת"

#: powerdevilcore.cpp:621
#, kde-format
msgid "Battery level critical. Your computer will go to sleep in 60 seconds."
msgstr "מצב הסוללה קריטי. המחשב ייכנס למצב שינה בעוד 60 שניות."

#: powerdevilcore.cpp:623
#, kde-format
msgctxt ""
"@action:button Suspend to ram without waiting for the battery critical timer"
msgid "Sleep Now"
msgstr "לישון כעת"

#: powerdevilcore.cpp:629
#, kde-format
msgid "Please save your work."
msgstr "נא לשמור את העבודה שלך."

#: powerdevilcore.cpp:635
#, kde-format
msgctxt ""
"Cancel timeout that will automatically put system to sleep because of low "
"battery"
msgid "Cancel"
msgstr "ביטול"

#: powerdevilcore.cpp:647
#, kde-format
msgid "Battery Low (%1% Remaining)"
msgstr "עוצמת הסוללה נמוכה (נותרו %1% אחוז)"

#: powerdevilcore.cpp:651
#, kde-format
msgid "Battery Critical (%1% Remaining)"
msgstr "עוצמת הסוללה במצב קריטי (נותרו %1% אחוז)"

#: powerdevilcore.cpp:675
#, kde-format
msgid "AC Adapter Plugged In"
msgstr "המטען מחובר"

#: powerdevilcore.cpp:678
#, kde-format
msgid "Running on AC power"
msgstr "פועל על מטען"

#: powerdevilcore.cpp:678
#, kde-format
msgid "The power adapter has been plugged in."
msgstr "המטען חובר."

#: powerdevilcore.cpp:681
#, kde-format
msgid "Running on Battery Power"
msgstr "פועל על סוללה"

#: powerdevilcore.cpp:681
#, kde-format
msgid "The power adapter has been unplugged."
msgstr "המטען נותק."

#: powerdevilcore.cpp:742
#, kde-format
msgid "Charging Complete"
msgstr "הטעינה הושלמה"

#: powerdevilcore.cpp:742
#, kde-format
msgid "Battery now fully charged."
msgstr "הסוללה טעונה במלואה כעת."

#~ msgctxt "Brightness level, label for the slider"
#~ msgid "Level"
#~ msgstr "רמה"

#~ msgid " min"
#~ msgstr " דק׳"

#~ msgid "After"
#~ msgstr "אחרי"

#~ msgid "Switch off after"
#~ msgstr "כיבוי לאחר"

#~ msgid " sec"
#~ msgstr " שנ׳"

#~ msgid "When screen is locked, switch off after"
#~ msgstr "כאשר המסך נעול, לכבות אותו לאחר"

#~ msgctxt ""
#~ "Execute action on lid close even when external monitor is connected"
#~ msgid "Even when an external monitor is connected"
#~ msgstr "אפילו כשמסך חיצוני מחובר"

#~ msgid "Do nothing"
#~ msgstr "לא לעשות כלום"

#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "לישון"

#~ msgid "Hibernate"
#~ msgstr "להירדם"

#~ msgid "Shut down"
#~ msgstr "לכבות"

#~ msgid "Lock screen"
#~ msgstr "לנעול מסך"

#~ msgid "Prompt log out dialog"
#~ msgstr "להציג חלון יציאה מהמערכת"

#~ msgid "Turn off screen"
#~ msgstr "כיבוי מסך"

#~ msgid "When laptop lid closed"
#~ msgstr "כאשר מכסה המחשב הנייד סגור"

#~ msgid "When power button pressed"
#~ msgstr "כאשר כפתור ההפעלה נלחץ"

#~ msgctxt "@label:slider Brightness level"
#~ msgid "Level"
#~ msgstr "רמה"

#~ msgid "Leave unchanged"
#~ msgstr "להשאיר ללא שינוי"

#~ msgid "Power Save"
#~ msgstr "חיסכון בחשמל"

#~ msgid "Balanced"
#~ msgstr "מאוזן"

#~ msgid "Performance"
#~ msgstr "ביצועים"

#~ msgctxt "Switch to power management profile"
#~ msgid "Switch to:"
#~ msgstr "מעבר אל:"

#~ msgid "Script"
#~ msgstr "סקריפט"

#~ msgid "When entering power state"
#~ msgstr "בכניסה למצב כיבוי"

#~ msgid "When exiting power state"
#~ msgstr "ביציאה ממצב כיבוי"

#~ msgid "After a period of inactivity"
#~ msgstr "לאחר משך זמן של חוסר פעילות"

#~ msgid "Run script"
#~ msgstr "הרצת סקריפט"

#~ msgid "after "
#~ msgstr "אחרי "

#~ msgid "Automatically"
#~ msgstr "אוטומטית"

#~ msgid "Standby (Save session to memory)"
#~ msgstr "השהיה (שמירת ההפעלה לזיכרון)"

#~ msgid "Hybrid sleep (Save session to both memory and disk)"
#~ msgstr "שינה לסירוגין (לשמור את ההפעלה לזיכרון ולכונן)"

#~ msgid "Standby, then hibernate after a period of inactivity"
#~ msgstr "השהיה, לאחר מכן לעבור לתרדמת לאחר משך זמן של חוסר פעילות"

#~ msgctxt ""
#~ "@label:combobox Sleep mode selection - suspend to memory, disk or both"
#~ msgid "When sleeping, enter"
#~ msgstr "במהלך שינה, להיכנס"

#~ msgid "The computer will shut down in 60 seconds."
#~ msgstr "המחשב ייכבה תוך 60 שניות."

#~ msgid "The computer will hibernate in 60 seconds."
#~ msgstr "המחשב יירדם תוך 60 שניות."

#~ msgid "The computer will sleep in 60 seconds."
#~ msgstr "המחשב יישן תוך 60 שניות."

#~ msgid "Hybrid sleep"
#~ msgstr "לישון לסירוגין"

#~ msgid "On Profile Load"
#~ msgstr "בטעינת הפרופיל"

#~ msgid "On Profile Unload"
#~ msgstr "בביטול הפרופיל"

#~ msgid "Unsupported suspend method"
#~ msgstr "סוג השהייה לא נתמך"

#, fuzzy
#~| msgid ""
#~| "Your battery is low. If you need to continue using your computer, either "
#~| "plug in your computer, or shut it down and then change the battery."
#~ msgid ""
#~ "Battery running low - to continue using your computer, plug it in or shut "
#~ "it down and change the battery."
#~ msgstr ""
#~ "הסוללה שלך חלשה. אם אתה צריך תמשיך להשתמש במחשב, אחרת חבר את המחשב למטען "
#~ "או כבה אותו"

#~ msgid "Turn off"
#~ msgstr "כבה"

#~ msgid "Turn on"
#~ msgstr "הדלק"

#~ msgid "Wi-Fi"
#~ msgstr "אינטרנט אלחוטי"

#~ msgid "Mobile broadband"
#~ msgstr "פס רחב נייד"

#~ msgid "Bluetooth"
#~ msgstr "בלוטוס"

#~ msgid "Can't open file"
#~ msgstr "לא יכול לפתוח את הקובץ"

#, fuzzy
#~| msgid ""
#~| "No valid Power Management backend plugins are available. A new "
#~| "installation might solve this problem."
#~ msgid ""
#~ "No valid Power Management backend plugins available. A new installation "
#~ "might solve this problem."
#~ msgstr "אין תוספי ניהול צריכת חשמל מותקנים, התקנה מחדש יכולה לפתור את זה."

#, fuzzy
#~| msgid ""
#~| "The profile \"%1\" has been selected, but it does not exist.\n"
#~| "Please check your PowerDevil configuration."
#~ msgid ""
#~ "Profile \"%1\" has been selected but does not exist.\n"
#~ "Please check your PowerDevil configuration."
#~ msgstr ""
#~ "הפרופיל \"%1\" נבחר, אבל אינו קיים.\n"
#~ "אנא בדוק את את הגדרות PowerDevil."

#~ msgid ""
#~ "Could not connect to battery interface.\n"
#~ "Please check your system configuration"
#~ msgstr ""
#~ "לא ניתן לתקשר עם ממשק הסוללה.\n"
#~ "אנא בדוק את הגדרות המערכת"

#, fuzzy
#~| msgid ""
#~| "KDE Power Management System could not be initialized. The backend "
#~| "reported the following error: %1\n"
#~| "Please check your system configuration"
#~ msgid ""
#~ "The KDE Power Management System could not be initialized. The backend "
#~ "reported the following error: %1\n"
#~ "Please check your system configuration"
#~ msgstr ""
#~ "מנהל החשמל של KDE לא יכול להדלק. השגיאה המדווחת: %1\n"
#~ "בדוק את הגדרות המערכת"

#~ msgid "All pending suspend actions have been canceled."
#~ msgstr "כל ההכנות לכניסה למצב השהייה בוטלו."

#~ msgctxt "Placeholder is device name"
#~ msgid ""
#~ "The battery in your keyboard (\"%1\") is low, and the device may turn "
#~ "itself off at any time. Please replace or charge the battery as soon as "
#~ "possible."
#~ msgstr ""
#~ "הסוללה במקלדת שלך (\"%1\") נמוכה, ואולי היא תתכבה בכל רגע. אנא החלף "
#~ "סוללות או הטען אותה בהקדם האפשרי"

#~ msgctxt "Placeholder is device name"
#~ msgid ""
#~ "The battery in a connected device (\"%1\") is low, and the device may "
#~ "turn itself off at any time. Please replace or charge the battery as soon "
#~ "as possible."
#~ msgstr ""
#~ "הסוללה בהתקן שלך (\"%1\") נמוכה, ואולי הוא יכבה את עצמו בכל רגע. אנא החלף "
#~ "סוללות או הטען אותו בהקדם האפשרי"

#~ msgid "Suspend"
#~ msgstr "השהיה לזכרון"
